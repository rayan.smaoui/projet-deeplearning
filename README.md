# Projet DeepLearning

The goal of this project is to allow everyone to detect if there is a possibility of getting a ransomware attack on their device. We are using DeepLearning models to try to accomplish this feat. You'll be able to find the notebook I used to train my models in this Git.

Here is the link to get the data and the model I trained : 
https://drive.google.com/drive/folders/1HLOiFQ8FE1kJ64fk_IWIkgXcw93j99R7?usp=sharing
